# Route Guide

## Requirements

- grpcio-tools

```
$ pip install grpcio-tools
```

## Run

Create python code from proto.

```
python -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. route_guide.proto
```
